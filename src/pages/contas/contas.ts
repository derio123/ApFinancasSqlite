import { Component } from '@angular/core';
import {
  NavController,
  NavParams, ModalController
} from 'ionic-angular';
import { ModalContasPage } from '../modal-contas/modal-contas';

@Component({
  selector: 'page-contas',
  templateUrl: 'contas.html',
})
export class ContasPage {

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContasPage');
  }

  adicionar() {
    let modal = this.modalCtrl.create(ModalContasPage);
    modal.present();
  }
}
