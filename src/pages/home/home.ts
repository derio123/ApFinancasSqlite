import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SaldoPage } from '../saldo/saldo';
import { ContasPage } from '../contas/contas';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  saldo(){
    this.navCtrl.push(SaldoPage);
  }
  contas(){
    this.navCtrl.push(ContasPage);
  }


}
